// ---------------------------------
// Grid View Toggling
// ---------------------------------
var iframe = document.getElementById('my-iframe');

(function () {
  var body = document.querySelectorAll('body');
  var gridToggle = document.querySelectorAll('.js-toggle-grid-view');

  // toggle local storage value + update state
  var toggleGridView = function () {
    var currentGridViewStatus = localStorage.getItem('gridView');
    if (currentGridViewStatus == null) {
      localStorage.setItem('gridView', 1);
      setGridView();
    } else {
      localStorage.removeItem('gridView');
      setGridView();
    }
  }

  // set state
  var setGridView = function () {
    var currentGridViewStatus = localStorage.getItem('gridView');
    if (currentGridViewStatus == 1) {
      body[0].classList.add('grid-view');
      gridToggle[1].setAttribute('aria-checked', 'true');
    } else {
      body[0].classList.remove('grid-view');
      gridToggle[1].setAttribute('aria-checked', 'false');
    }
  }

	// init
	if (gridToggle.length !== 0) {
		document.addEventListener('DOMContentLoaded', (event) => {
			setGridView();
		});

		// listen for user input
		gridToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				toggleGridView();
			});
		});
	}


  // ---------------------------------
  // Debug View Toggling
  // ---------------------------------

  var debugToggle = document.querySelectorAll('.js-toggle-debug-view');

  // toggle local storage value + update state
  var toggleDebugView = function () {
    var currentDebugViewStatus = localStorage.getItem('debugView');
    if (currentDebugViewStatus == null) {
      localStorage.setItem('debugView', 1);
      setDebugView();
    } else {
      localStorage.removeItem('debugView');
      setDebugView();
    }
  }

  // set state
  var setDebugView = function () {
    var currentDebugViewStatus = localStorage.getItem('debugView');
	var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;

    if (currentDebugViewStatus == 1) {
	  frameDocument.querySelector('body').classList.add('debug-view');
	  if (debugToggle[1] != null) {
			debugToggle[1].setAttribute('aria-checked', 'true');
	  }

    } else {
      frameDocument.querySelector('body').classList.remove('debug-view');
			if (debugToggle[1] != null) {
				debugToggle[1].setAttribute('aria-checked', 'false');
			}
    }
  }

	// listen for user input
	if (debugToggle.length !== 0) {
		debugToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				toggleDebugView();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setDebugView();
		});
	}


  // ---------------------------------
  // Pin Nav Toggling
  // ---------------------------------
  var pinToggle = document.querySelectorAll('.js-toggle-pin-nav');

  // toggle local storage value + update state
  var togglePinNav = function () {
    var currentPinNavStatus = localStorage.getItem('pinNav');
    if (currentPinNavStatus == null) {
      localStorage.setItem('pinNav', 1);
      setPinNav();
    } else {
      localStorage.removeItem('pinNav');
      setPinNav();
    }
  }

  // set state
  var setPinNav = function () {
    var currentPinNavStatus = localStorage.getItem('pinNav');
    if (currentPinNavStatus == 1) {
      body[0].classList.add('pin-nav');
      pinToggle[1].setAttribute('aria-checked', 'true');
    } else {
      body[0].classList.remove('pin-nav');
      pinToggle[1].setAttribute('aria-checked', 'false');
    }
  }

	// listen for user input
	if (pinToggle.length !== 0) {
		pinToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				togglePinNav();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setPinNav();
		});
	}

  // ---------------------------------
  // Theme Switching
  // ---------------------------------
  var themeSwitchToggle = document.querySelectorAll('.js-toggle-theme-switch');

  // toggle local storage value + update state
  var themeSwitchNav = function () {
    var currentThemeSwitchStatus = localStorage.getItem('themeSwitch');
    if (currentThemeSwitchStatus == null) {
      localStorage.setItem('themeSwitch', 1);
      setThemeSwitch();
    } else {
      localStorage.removeItem('themeSwitch');
      setThemeSwitch();
    }
  }

  // set state
  var setThemeSwitch = function () {
		var iframeArray = [];
		var currentThemeSwitchStatus = localStorage.getItem('themeSwitch');
		if (document.body.contains(document.getElementById('my-iframe'))) {
			iframe = document.getElementById('my-iframe');

			// if (iframe.classList.contains(document.getElementsByClassName('docs-component__preview'))) {
			// 	iframeArray = document.getElementsByClassName('docs-component__preview');
			// }
		}
		else {
			return;
		}
		var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
		// var frameDocumentNested = frameDocument.contentDocument ? frameDocument.contentDocument : frameDocument.contentWindow.document;
		// console.log(frameDocumentNested);
		var stylesheetPrimary = frameDocument.getElementById("stylesheet-primary");
		var stylesheetSecondary = frameDocument.getElementById("stylesheet-secondary");

		// check if there is a page/component, that can be switched
		var stylesheetTogglingAvailable = false;
		if (stylesheetPrimary != null && stylesheetSecondary != null) {
			stylesheetTogglingAvailable = true;
		}

    if (!stylesheetTogglingAvailable ) {
			themeSwitchToggle[1].setAttribute('disabled', '');
		} else {
			themeSwitchToggle[1].removeAttribute('disabled');
			if (currentThemeSwitchStatus == 1) {
				// body[0].classList.add('pin-nav');
				stylesheetPrimary.disabled = true;
				stylesheetSecondary.disabled = false;
				themeSwitchToggle[1].setAttribute('aria-checked', 'true');
			} else {
				// body[0].classList.remove('pin-nav');
				stylesheetPrimary.disabled = false;
				stylesheetSecondary.disabled = true;
				themeSwitchToggle[1].setAttribute('aria-checked', 'false');
			}
		}
  }

	if (themeSwitchToggle.length !== 0) {
		// listen for user input
		themeSwitchToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				themeSwitchNav();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setThemeSwitch();
		});
	}


  // init in iframe
  var handleLoad = () => {
		if (debugToggle.length !== 0) {
			setDebugView();
		}
		if (themeSwitchToggle.length !== 0) {
			setThemeSwitch();
		}

    var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
    var iframeTitle = frameDocument.getElementsByTagName("title")[0].innerText;
    document.title = iframeTitle;

    var element = document.createElement("link");
    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", "../styleguide/css/sk-styleguide-dev-suite.css"); // todo dynamically
    frameDocument.head.appendChild(element);
  };

  if (iframe) {
    iframe.addEventListener('load', handleLoad, true);
  }
})();
