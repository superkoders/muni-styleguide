import { setUrl, dissectUrl } from './url-manager';

// ---------------------------------
// nav keyboard navigation and
// active template highlighting
// ---------------------------------


let links = document.querySelectorAll('.js-nav-iframe li a:not(.js-nav-accordion__toggle)');
let validLinks = [];
let navigate;
// separate inactive and valid links
links.forEach(function (link) {
  validLinks.push(link);
});

const enhanceNav = () => {
  // when changing active link, remove is-active class from the last one
  const activeLink = document.querySelectorAll('.js-nav-iframe li a.is-active');
  if (activeLink.length) {
    activeLink[0].classList.remove('is-active');
  }

  // check which template is active
  let currentUrl = window.location.href; // whole url in omnibox
  let currentPage = dissectUrl(currentUrl).pageParameter; // parameter page

  // determine active link and its position in an array
  let currentLink = undefined;
  let currentLinkPos = undefined;
  validLinks.forEach(function (validLink, i) {
    if (validLink.href === currentPage) {
      currentLink = validLink;
      currentLinkPos = i;
      currentLink.classList.add("is-active");
    }
  })

  if (document.querySelectorAll(".js-nav-keyboard").length) {
    let prevLink = validLinks[currentLinkPos - 1];
    let nextLink = validLinks[currentLinkPos + 1];

    // keyboard controls
    navigate = function navigate(e) {
      if (e.which == 37) {
        if (prevLink && (currentLinkPos > 0)) {
          setUrl(prevLink.href);
        }
      }
      if (e.which == 39) {
        if (nextLink) {
          setUrl(nextLink.href);
        }
      }
    }
  } else {
    console.info("Keyboard template navigation is disabled. You can enable it in package settings. See gulp-sk-styleguide documentation on npm.");
  }
};
window.addEventListener('keydown', function () {
  navigate(event)
});
enhanceNav();

// when url changes, update navigation's active styles and pick corresponding prev/next links
window.addEventListener('urlChange', enhanceNav);
