# Muni styleguide generator
Plugin for generating styleguide from static templates. It is fork of [sk-styleguide](https://bitbucket.org/superkoders/sk-styleguide/src/master/).
It doens't build component documentation, because that is handled separetely inside project.

## Author

[![N|Superkoders](http://logo.superkoderi.cz/superkoders.svg)](https://www.superkoders.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
