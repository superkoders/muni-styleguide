// Copies styleguide assets to project's folder

const { src, dest } = require('gulp');
const path = require('path');

module.exports = (packageOptions) => function copyAssets() {
	const inputPath1 = (path.resolve(__dirname, '../dist/**/*'));
	const inputPath2 = (path.resolve(__dirname, '../dist/tpl'));
	const inputPath3 = (path.resolve(__dirname, '../dist/**/*.njk'));

	return src([
		inputPath1,
		'!' + inputPath2,
		'!' + inputPath3,
	])
		.pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder)));
};
