// 1. take user settings and check, whether it is valid.
// 2. build an object with info, whether paths do really exist
// 3. return the updated object for the other tasks

'use strict';

const fs = require('fs');
const fm = require('front-matter');
const path = require('path');
// const { isObject, iterationCopy } = require('./deepcopy'); // this converts nested array to object :/

module.exports = function validity(options) {
    const filePaths = options.docs;
    const data = Object.keys(filePaths).reduce((acc, key) => {
        const file = filePaths[key];
        let isExist = false;
        let fileName = '';
        try {
            fs.readFileSync(file, { encoding: 'utf-8' });
            isExist = true;
            fileName = file.split('/');
            fileName = fileName.slice(fileName.length - 1);
            fileName = fileName.join('/').split('.').slice(0, -1).join('.');
        } catch (err) {
            if (file !== options.docs.codeGuidelinesSnippetsFolder) {
                console.log(file + ' is not valid. Skipping.');
            }
        }
        return [...acc, { key, file, isExist, fileName }];
    }, []);

    let checkedOptions = options;
    checkedOptions.docsPaths = data;

    let projectFiles = [];

    fs.readdirSync(path.resolve('.', options.publicPath, options.templatesFolder)).forEach(file => {
		projectFiles.push(file);
    });
	checkedOptions.projectFiles = projectFiles;

    let projectMetaComponents = [];

    fs.readdirSync(path.resolve('.', options.publicPath, options.metaComponentsFolder)).forEach(file => {
		projectMetaComponents.push(file);
		});
	checkedOptions.projectMetaComponents = projectMetaComponents;




	let componentObject = {}; // object with info about component and its states
	let componentObjectList = []; // array of all component objects sorted in categories
	let blocks = {}; // component category
	blocks.array = []; // component category
	let crossroads = {}; // component category
	crossroads.array = []; // component category
	let forms = {}; // component category
	forms.array = []; // component category
	let pickers = {}; // component category
	pickers.array = []; // component category
	let i = {}; // component category
	i.array = []; // component category
	let menu = {}; // component category
	menu.array = []; // component category
	let comps = {}; // component category
	comps.array = []; // component category
	let includeDataSnippet; // include snippet - json without wrapping {}
	let includeNestedDataSnippet; // include snippet - json without wrapping {}
	let componentVariant = {}; // object with additional data of component variant
	let componentVariants = []; // array of all variant objects

	// take path to the folder with components and iterate inside it. Get info about default components.
	let componentsParentFolderPath = fs.readdirSync(path.resolve('.', options.componentsFolder));
	componentsParentFolderPath.forEach(folder => {
		if (folder === ('.DS_Store' || '.thumbs')) {
			return;
		}

		// inicialize variables
		let includeData;
		let includeNestedData;

		// go inside each folder and gather more data
		let componentsFolderPath = fs.readdirSync(path.resolve('.', options.componentsFolder, folder));
		componentsFolderPath.forEach(file => {
			componentVariants = []; // flush componentVariants array before looping in another folder, so it doesn't sum all variants for all components

			// check for variants inside nested folder 'data'
			let componentDataFolderPath = path.resolve('.', options.componentsFolder, folder, 'data');
			if (fs.existsSync(componentDataFolderPath)) {
				let componentDataFolderPath = fs.readdirSync(path.resolve('.', options.componentsFolder, folder, 'data'));
				componentDataFolderPath.forEach(file => {
					if (file === ('.DS_Store' || 'Thumbs.db')) {
						return;
					}
					var ext = file.substr(file.lastIndexOf('.') + 1);
					if (ext != 'json') {
						return;
					}

					let filePath = path.resolve('.', options.componentsFolder, folder, 'data', file);
					if (ext == 'json') {

						let variantName = file.split('.').slice(0, -1);
						variantName = variantName[0];

						componentVariant = {}; // flush componentVariant object, so it doesn't sum all variants for all components

						let fileData = '';
						const fileContents = String(fs.readFileSync(filePath, { encoding: 'utf-8' }));

						function tryParseJSON (jsonString){
							try {
								fileData = JSON.parse(jsonString);
								// Handle non-exception-throwing cases:
								// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
								// but... JSON.parse(null) returns null, and typeof null === "object",
								// so we must check for that, too. Thankfully, null is falsey, so this suffices:
								if (fileData && typeof fileData === "object") {
									fileData = true;
									return fileData;
								} else {
									// console.log("else: invalid JSON!");
								}
							}
							catch (e) {
								fileData = false;
								// console.log("catch: invalid JSON!");
							}
							return false;
						};
						tryParseJSON(fileContents);

						includeNestedData = JSON.parse(JSON.stringify(fs.readFileSync(filePath, { encoding: 'utf-8' })));
						// remove enclosing { and } from json snippet
						// for better alignement of snippet we want to include only direct content in json, so it matches twig notation
						includeNestedDataSnippet = JSON.stringify(includeNestedData);
						includeNestedDataSnippet = includeNestedDataSnippet.replace(/("{\\n|"{|}"|}\\n"|\\n}\\n")/gi,'"');
						includeNestedDataSnippet = JSON.parse(includeNestedDataSnippet);
						// check for empty snippets
						if (includeNestedDataSnippet == '\n' || includeNestedDataSnippet == '') {
							includeNestedDataSnippet = null;
						};

						if (fileData == false) {
							componentVariant.validData = false;
						} else {
							componentVariant.validData = true;
						}
						componentVariant.variantName = variantName;
						componentVariant.includeNestedData = includeNestedData;
						componentVariant.includeNestedDataSnippet = includeNestedDataSnippet;
						componentVariants.push(JSON.parse(JSON.stringify(componentVariant)));
					}
				})
				componentObject.error = null;
			} else {
				// console.log(folder + ' is missing data folder, documentation can lack needed information.');
				componentObject.error = `No example data in ${path.join(options.componentsFolder, folder, 'data')} specified`;
				return;
			}
		})

		// set output values
		componentObject.title = folder; // set component title
		componentObject.includeData = includeData; // set include data for example snippet
		componentObject.includeDataSnippet = includeDataSnippet; // set include data for example snippet
		componentObject.componentVariants = componentVariants; // set object with variants data
		// console.log(componentObject);

		// sort components and then push gathered info to the category inside master object; use JSON hack to make a deep copy
		switch (folder.substring(0,2)) {
			case 'b-':
				blocks.name = 'Box';
				blocks.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			case 'c-':
				crossroads.name = 'Crossroad';
				crossroads.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			case 'i-':
				i.name = 'Inputs';
				i.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			case 'p-':
				pickers.name = 'Pickers';
				pickers.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			case 'f-':
				forms.name = 'Forms';
				forms.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			case 'm-':
				menu.name = 'Menu';
				menu.array.push(JSON.parse(JSON.stringify(componentObject)));
				break;
			default:
				comps.name = 'Core';
				comps.array.push(JSON.parse(JSON.stringify(componentObject)));
		}
	});

	if (blocks.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(blocks)));
	}
	if (crossroads.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(crossroads)));
	}
	if (i.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(i)));
	}
	if (pickers.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(pickers)));
	}
	if (forms.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(forms)));
	}
	if (menu.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(menu)));
	}
	if (comps.array.length > 0) {
		componentObjectList.push(JSON.parse(JSON.stringify(comps)));
	}

	// console.log(componentObjectList);
	// console.log(componentObjectList.blocks);
	let componentProperties = [];
	checkedOptions.componentObjectList = componentObjectList;

    return checkedOptions;
}
