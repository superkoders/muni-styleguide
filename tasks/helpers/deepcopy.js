const isObject = function isObject(obj) {
	var type = typeof obj;
	return type === 'function' || type === 'object' && !!obj;
};

const iterationCopy = function iterationCopy(src) {
	let target = {};
	for (let prop in src) {
		console.log('--------------------------');
		console.log(src, 'src');
		console.log(prop, 'prop');
		console.log('..........................');
	  if (src.hasOwnProperty(prop)) {
			console.log(src, prop, ' 1 if: src hasOwnProperty');
			console.log('..........................');
			// if the value is a nested object, recursively copy all it's properties
			if (isObject(src[prop])) {
				console.log('2 if');
				console.log(src[prop]);
				console.log('..........................');
				target[prop] = iterationCopy(src[prop]);
			} else {
				console.log('2 else');
				console.log(src[prop]);
				console.log('..........................');
				target[prop] = src[prop];
			}
	  }
		console.log('==========================');
	}
	return target;
};

module.exports = { isObject, iterationCopy };


// { title: 'accordion',
//   includeData: { items: [ [Object], [Object] ] },
//   variations:
//    [ { title: 'accordion', includeData: 'ahoj' },
//      { title: 'accordion2', includeData: 'ahoj' } ] }

// Co kurva potrebuju - objekt, reprezentujici komponentu a vsechny jeji stavy
// {
//  title - mozna nutne neni treba, ale pro jistotu a konzistenci
//	includeData - default - JSON objekt
//  varianty: - pole
//    -
//		  nazev varianty
//		  specificka include data - JSON objekt
//    -
//		  nazev varianty
//		  specificka include data
// }
