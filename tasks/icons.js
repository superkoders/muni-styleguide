// 1. reads project's icons
// 2. builds list of them
// 3. outputs the result to the project's folder

'use strict';

const { src, dest } = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const fs = require('fs');
const path = require('path')

module.exports = (packageOptions) => function icons(done) {
  let icons = [];
  let noIcons = true;
  let userIconDocsName = false;
  if (packageOptions.settings.iconList) {
    try {
      fs.readdirSync(path.resolve('.', packageOptions.iconsFolder)).forEach(file => {
        if (file === '.DS_Store') {
          return;
        }
        file = file.split('.').slice(0, -1).join('.');
        icons.push(file);
        noIcons = false;
      });
    } catch (err) {
      console.log(packageOptions.iconsFolder + ' is not valid. Skipping.');
    }
  }

  if (packageOptions.docs.icons) {
    packageOptions.docsPaths.filter(function (file) {
      if (file.isExist && (file.key === 'icons')) {
        userIconDocsName = file.fileName;
      }
    });
  }

  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
    done();
  };

  if (packageOptions.settings.iconList) {
    return src([
      'icons.njk',
    ], {
      cwd: path.resolve(__dirname + '/../src/docs/'),
    })
      .pipe(plumber({
        errorHandler: onError,
      }))
      .pipe(nunjucksRender({
        path: path.resolve(__dirname + '/../src/docs/'),
        data: {
          extractedIcons: icons,
          noIcons: noIcons,
          userIconDocsName: userIconDocsName,
          config: packageOptions
        },
      }))
      .pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder)));
  }
  else {
    done();
  }
};
