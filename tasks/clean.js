const del = require('del');
const path = require('path');

module.exports = function clean() {
  return del([path.resolve(__dirname, '../src/docs/userDocs/')], { force: true });
};
