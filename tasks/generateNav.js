// 1. builds styleguide skeleton and outputs it to the project's folder

'use strict';

const { src, dest } = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const fs = require('fs');
const path = require('path')

module.exports = (packageOptions) => function generateNav() {
	const onError = function (error) {
		notify.onError({
			title: 'Styleguide error!',
			message: '<%= error.message %>',
			sound: 'Beep',
		})(error);
	};

	return src([
		'*.njk',
	], {
		cwd: path.resolve(__dirname + '/../src/'),
	})
		.pipe(plumber({
			errorHandler: onError,
		}))
		.pipe(nunjucksRender({
			path: path.resolve(__dirname + '/../src/'),
			data: {
				relativeDest: path.join(packageOptions.relativePath, packageOptions.templatesFolder),
				relativeMetaComponentsDest: path.join(packageOptions.relativePath, packageOptions.metaComponentsFolder),
				config: packageOptions
			},
		}))
		.pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder)));
};
