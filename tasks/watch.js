const gulp = require('gulp');
const notifier = require('node-notifier');
const { series, watch: watchGulp } = gulp;

module.exports = function watch(done) {
	return series(
		function watching() {

			watchGulp('./src/js/**/*.js', require('./webpack'));
			watchGulp('./src/scss/**/*.scss', require('./sass'));

			notifier.notify({
				'title': 'Start Project',
				'message': 'Gulp is watching files.',
				'sound': 'Hero',
			});
		},
	)(done);
};
