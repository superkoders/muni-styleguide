// 1. builds styleguide pages and outputs them to the project's folder

'use strict';

const { src, dest } = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const fs = require('fs');
const path = require('path');

module.exports = (packageOptions) => function generateDocs() {
  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
	};


// [{
// 		title: 'accordion',
//      includeData: { items: [Array] },
//      variations: [
//		   {
//			   'accordion',
//			   icnludedeData: ''
//		   }
//		   'accordion2'
//		] },
//   {
//	    title: 'article-teaser',
//      includeData: {},
//      variations: [ 'default' ] },
//   {
// 	title: 'button', includeData: {}, variations: [ 'default' ] },
//   {
// 	title: 'crossroad',
//     includeData: {},
// 	variations: [ 'default' ]
//   }]

	packageOptions.componentObjectList.forEach(component => {
		return src([
			'component-detail.njk',
		], {
			cwd: path.resolve(__dirname + '/../src/docs/'),
		})
			.pipe(plumber({
				errorHandler: onError,
			}))
			.pipe(nunjucksRender({
				path: path.resolve(__dirname + '/../src/docs/'),
				data: {
					relativeDest: './',
					config: packageOptions,
					component: component
				},
		}))
		.pipe(
			rename({
				basename: 'index',
				extname: '.html',
			}),
		)
			.pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder, 'components/', component.title)));
	})
};
