// 1. copies and escapes snippets to userDocs folder i needed
// 2. other tasks picks that up for other processing

'use strict';

const { src, dest } = require('gulp');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const path = require('path')
const escape = require('gulp-html-escape');

module.exports = (packageOptions) => function copySnippets(done) {

  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
  };

  // check if user turned CG on and specified path to snippets folder
  let codeguidelinesActivated;
  let srcPath;

  packageOptions.docsPaths.filter(function(item) {
    if (item.key === 'codeGuidelines') {
      return codeguidelinesActivated = item.isExist;
    }
    if (item.key === 'codeGuidelinesSnippetsFolder') {
      return srcPath = item.file;
    }
  });

  if (codeguidelinesActivated && (srcPath != undefined)) {
    return src([
			'*.html',
    ], {
      cwd: path.resolve('.', srcPath),
    })
    .pipe(plumber({
      errorHandler: onError,
    }))
    .pipe(escape())
    .pipe(dest(path.resolve(__dirname, '../src/docs/userDocs/snippets')));
  }
    else if (codeguidelinesActivated) {
    done();
    console.log("Path to the snippets is not correct or empty. Skipping.");
  }
    else if (srcPath != undefined) {
    console.log("Code guidelines not activated. Specify valid src path.");
    done();
  }
    else {
    // guidelines are off, do nothing
     done();
  }

	// // TODO: extract with function above
	// return src([
	// 	'**/*.html',
	// ], {
	// 	cwd: path.resolve('.', packageOptions.publicPath, 'markupDocs'), // TODO: dynamic
	// })
	// .pipe(plumber({
	// 	errorHandler: onError,
	// }))
	// .pipe(escape())
	// .pipe(dest(path.resolve(__dirname, '../src/docs/componentsDocs/snippets')));
};
