const path = require('path');
const notify = require('gulp-notify');
const bundler = require('webpack');

module.exports = function webpack(callback) {
  var isReady = false;
  var settings = {
    mode: 'development',
    resolve: {
      extensions: ['.js', '.json', '.jsx'],
      modules: [
        path.join(__dirname, '../node_modules'),
      ],
    },
    devtool: 'none',
    entry: {
      styleguide: './src/js/styleguide',
      styleguideIframe: './src/js/styleguide-iframe',
    },
    output: {
      path: path.join(__dirname, '../dist/js/'),
      filename: '[name].js',
      chunkFilename: '[name].chunk.js',
    },
    module: {
      rules: [
        {
          test: /\.js(x)?$/,
          exclude: function(modulePath) {
						return !/node_modules/.test(modulePath) && !/node_modules\/@superkoders/.test(modulePath);
          },
          use: 'babel-loader',
        },
      ],
    },
    profile: true,
    watchOptions: {
      ignored: /node_modules/,
    },
  };

  var onError = notify.onError(function (error) {
    return {
      title: 'JS error!',
      message: error,
      sound: 'Beep',
    };
  });

  var bundle = bundler(settings, function (error, stats) {
    var jsonStats = stats.toJson();
    var errors = jsonStats.errors;
    var warnings = jsonStats.warnings;

    if (error) {
      onError(error);
    } else if (errors.length > 0) {
      onError(errors.toString());
    } else if (warnings.length > 0) {
      onError(warnings.toString());
    } else {
      // console.log('webpack problem: ' + error); TODO
    }

    if (!isReady) {
      callback();
    }

    return isReady = true;
  });

  return bundle;
};
