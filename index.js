const { series } = require('gulp');
const deepmerge = require('deepmerge');
const generateNav = require('./tasks/generateNav.js');
const generateDocs = require('./tasks/generateDocs.js');
const generateComponentsDocs = require('./tasks/generateComponentsDocs.js');
const copyAssets = require('./tasks/copyAssets.js');
const icons = require('./tasks/icons.js');
const convertMarkdown = require('./tasks/convertMarkdown.js');
const convertYaml = require('./tasks/convertYaml.js');
const copySnippets = require('./tasks/copySnippets.js');
const generateHtmlSnippets = require('./tasks/generateHtmlSnippets.js');
const generateHtmlExamples = require('./tasks/generateHtmlExamples.js');
const validity = require('./tasks/helpers/checkValidity.js');
const clean = require('./tasks/clean.js');
const path = require('path');

const defaultOptions = {
	projectName: 'Project name',
	templatesFolder: 'tpl/',
	styleguideFolder: 'styleguide/',
	// iconsFolder: './src/img/bg/icons-svg/',
	publicPath: './web/',
	relativePath: '../', // todo: from template folder to publicPath
	componentsFolder: './src/twig/partials/',
	projectRoot: path.resolve('.'),
	settings: {
		arrowNav: true,
		pinNav: true,
		gridView: false,
		debugView: false,
		iconList: false,
		theme: 'light'
	}
};

const dev = (packageOptions) => {
	const generateNavTask = generateNav(packageOptions);
	const generateDocsTask = generateDocs(packageOptions);
	const generateComponentsDocsTask = generateComponentsDocs(packageOptions);
	// const generateHtmlSnippetsTask = generateHtmlSnippets(packageOptions);
	// const generateHtmlExamplesTask = generateHtmlExamples(packageOptions);
	const copyAssetsTask = copyAssets(packageOptions);
	const iconsTask = icons(packageOptions);
	const convertMarkdownTask = convertMarkdown(packageOptions);
	const convertYamlTask = convertYaml(packageOptions);
	const copySnippetsTask = copySnippets(packageOptions);

	return function dev(done) {
		return series(
			clean,
			series(
				copySnippetsTask,
				convertYamlTask,
				convertMarkdownTask,
				// generateHtmlSnippetsTask,
				// generateHtmlExamplesTask,
				generateDocsTask,
				generateNavTask,
				copyAssetsTask,
				iconsTask,
				// generateComponentsDocsTask, // TODO: dodelat callback, aby to mohlo byt kdekoli
			)
		)(done);
	};
};

module.exports = function defaultTask(userOptions, done) {
	const options = deepmerge(defaultOptions, userOptions);
	const checkedOptions = validity(options);

	return series(
		dev(checkedOptions)
	)(done);
};
