/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/styleguide.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@superkoders/sk-tools/src/array.js":
/*!*********************************************************!*\
  !*** ./node_modules/@superkoders/sk-tools/src/array.js ***!
  \*********************************************************/
/*! exports provided: first, last, length, clone, map, flatten, join, inArray, arrayOrEmpty, groupBy, emptyBySize */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "first", function() { return first; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "last", function() { return last; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "length", function() { return length; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clone", function() { return clone; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flatten", function() { return flatten; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "join", function() { return join; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "inArray", function() { return inArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrayOrEmpty", function() { return arrayOrEmpty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "groupBy", function() { return groupBy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "emptyBySize", function() { return emptyBySize; });
/* harmony import */ var _function__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./function */ "./node_modules/@superkoders/sk-tools/src/function.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var first = function first(arr) {
  return arr[0] || null;
};
var last = function last(arr) {
  return arr[arr.length - 1] || null;
};
var length = function length(arr) {
  return arr.length;
};
var clone = function clone(arr) {
  return [].slice.call(arr);
};
var map = Object(_function__WEBPACK_IMPORTED_MODULE_0__["curry"])(function (fn, arr) {
  return arr.map(fn);
});
var flatten = function flatten(arr) {
  return arr.reduce(function (acc, cur) {
    return acc.concat(cur);
  }, []);
};
var join = Object(_function__WEBPACK_IMPORTED_MODULE_0__["curry"])(function (delimiter, arr) {
  return arr.join(delimiter);
});
var inArray = function inArray(value, arr) {
  return arr.indexOf(value) > -1;
};
var arrayOrEmpty = function arrayOrEmpty(arr) {
  return arr || [];
};
var groupBy = Object(_function__WEBPACK_IMPORTED_MODULE_0__["curry"])(function (prop, arr) {
  return arr.reduce(function (acc, cur) {
    return _objectSpread({}, acc, _defineProperty({}, cur[prop], cur));
  }, {});
});
var emptyBySize = function emptyBySize(size) {
  return Array.apply(null, Array(size));
};

/***/ }),

/***/ "./node_modules/@superkoders/sk-tools/src/event.js":
/*!*********************************************************!*\
  !*** ./node_modules/@superkoders/sk-tools/src/event.js ***!
  \*********************************************************/
/*! exports provided: delegate, on, off */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delegate", function() { return delegate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "on", function() { return on; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "off", function() { return off; });
/* harmony import */ var _selector__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selector */ "./node_modules/@superkoders/sk-tools/src/selector.js");

var delegate = function delegate(selector, callback) {
  return function (event) {
    var node = Object(_selector__WEBPACK_IMPORTED_MODULE_0__["query"])(selector, event.currentTarget).find(function (node) {
      return node.contains(event.target);
    });

    if (node != null) {
      callback(event, node);
    }

    return event;
  };
};
var on = function on(node, type, handler) {
  node.addEventListener(type, handler, false);
};
var off = function off(node, type, handler) {
  node.removeEventListener(type, handler);
};

/***/ }),

/***/ "./node_modules/@superkoders/sk-tools/src/function.js":
/*!************************************************************!*\
  !*** ./node_modules/@superkoders/sk-tools/src/function.js ***!
  \************************************************************/
/*! exports provided: curry, pipe, tap, invoke, debounce */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "curry", function() { return curry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pipe", function() { return pipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tap", function() { return tap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invoke", function() { return invoke; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
var curry = function curry(f) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  if (args.length >= f.length) return f.apply(void 0, args);
  return function () {
    for (var _len2 = arguments.length, next = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      next[_key2] = arguments[_key2];
    }

    return curry.apply(void 0, [f.bind.apply(f, [f].concat(args))].concat(next));
  };
};
var pipe = function pipe() {
  for (var _len3 = arguments.length, fns = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    fns[_key3] = arguments[_key3];
  }

  return function (x) {
    return fns.reduce(function (v, f) {
      return f(v);
    }, x);
  };
};
var tap = curry(function (fn, arg) {
  fn(arg);
  return arg;
});
var invoke = curry(function (fnName, args, obj) {
  return obj[fnName].apply(obj, args);
});
var debounce = curry(function (wait, func) {
  var timeout;
  return function () {
    for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      args[_key4] = arguments[_key4];
    }

    var later = function later() {
      timeout = null;
      func.apply(void 0, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
});

/***/ }),

/***/ "./node_modules/@superkoders/sk-tools/src/selector.js":
/*!************************************************************!*\
  !*** ./node_modules/@superkoders/sk-tools/src/selector.js ***!
  \************************************************************/
/*! exports provided: byID, byName, query */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "byID", function() { return byID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "byName", function() { return byName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "query", function() { return query; });
/* harmony import */ var _array__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./array */ "./node_modules/@superkoders/sk-tools/src/array.js");

var byID = function byID(selector) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return [context.getElementById(selector)];
};
var byName = function byName(selector) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return Object(_array__WEBPACK_IMPORTED_MODULE_0__["clone"])(context.getElementsByName(selector));
};
var query = function query(selector) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return Object(_array__WEBPACK_IMPORTED_MODULE_0__["clone"])(context.querySelectorAll(selector));
};

/***/ }),

/***/ "./node_modules/decode-uri-component/index.js":
/*!****************************************************!*\
  !*** ./node_modules/decode-uri-component/index.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var token = '%[a-f0-9]{2}';
var singleMatcher = new RegExp(token, 'gi');
var multiMatcher = new RegExp('(' + token + ')+', 'gi');

function decodeComponents(components, split) {
  try {
    // Try to decode the entire string first
    return decodeURIComponent(components.join(''));
  } catch (err) {// Do nothing
  }

  if (components.length === 1) {
    return components;
  }

  split = split || 1; // Split the array in 2 parts

  var left = components.slice(0, split);
  var right = components.slice(split);
  return Array.prototype.concat.call([], decodeComponents(left), decodeComponents(right));
}

function decode(input) {
  try {
    return decodeURIComponent(input);
  } catch (err) {
    var tokens = input.match(singleMatcher);

    for (var i = 1; i < tokens.length; i++) {
      input = decodeComponents(tokens, i).join('');
      tokens = input.match(singleMatcher);
    }

    return input;
  }
}

function customDecodeURIComponent(input) {
  // Keep track of all the replacements and prefill the map with the `BOM`
  var replaceMap = {
    '%FE%FF': "\uFFFD\uFFFD",
    '%FF%FE': "\uFFFD\uFFFD"
  };
  var match = multiMatcher.exec(input);

  while (match) {
    try {
      // Decode as big chunks as possible
      replaceMap[match[0]] = decodeURIComponent(match[0]);
    } catch (err) {
      var result = decode(match[0]);

      if (result !== match[0]) {
        replaceMap[match[0]] = result;
      }
    }

    match = multiMatcher.exec(input);
  } // Add `%C2` at the end of the map to make sure it does not replace the combinator before everything else


  replaceMap['%C2'] = "\uFFFD";
  var entries = Object.keys(replaceMap);

  for (var i = 0; i < entries.length; i++) {
    // Replace all decoded components
    var key = entries[i];
    input = input.replace(new RegExp(key, 'g'), replaceMap[key]);
  }

  return input;
}

module.exports = function (encodedURI) {
  if (typeof encodedURI !== 'string') {
    throw new TypeError('Expected `encodedURI` to be of type `string`, got `' + _typeof(encodedURI) + '`');
  }

  try {
    encodedURI = encodedURI.replace(/\+/g, ' '); // Try the built in decoder first

    return decodeURIComponent(encodedURI);
  } catch (err) {
    // Fallback to a more advanced decoder
    return customDecodeURIComponent(encodedURI);
  }
};

/***/ }),

/***/ "./node_modules/query-string/index.js":
/*!********************************************!*\
  !*** ./node_modules/query-string/index.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var strictUriEncode = __webpack_require__(/*! strict-uri-encode */ "./node_modules/strict-uri-encode/index.js");

var decodeComponent = __webpack_require__(/*! decode-uri-component */ "./node_modules/decode-uri-component/index.js");

var splitOnFirst = __webpack_require__(/*! split-on-first */ "./node_modules/split-on-first/index.js");

function encoderForArrayFormat(options) {
  switch (options.arrayFormat) {
    case 'index':
      return function (key) {
        return function (result, value) {
          var index = result.length;

          if (value === undefined) {
            return result;
          }

          if (value === null) {
            return [].concat(_toConsumableArray(result), [[encode(key, options), '[', index, ']'].join('')]);
          }

          return [].concat(_toConsumableArray(result), [[encode(key, options), '[', encode(index, options), ']=', encode(value, options)].join('')]);
        };
      };

    case 'bracket':
      return function (key) {
        return function (result, value) {
          if (value === undefined) {
            return result;
          }

          if (value === null) {
            return [].concat(_toConsumableArray(result), [[encode(key, options), '[]'].join('')]);
          }

          return [].concat(_toConsumableArray(result), [[encode(key, options), '[]=', encode(value, options)].join('')]);
        };
      };

    case 'comma':
      return function (key) {
        return function (result, value, index) {
          if (value === null || value === undefined || value.length === 0) {
            return result;
          }

          if (index === 0) {
            return [[encode(key, options), '=', encode(value, options)].join('')];
          }

          return [[result, encode(value, options)].join(',')];
        };
      };

    default:
      return function (key) {
        return function (result, value) {
          if (value === undefined) {
            return result;
          }

          if (value === null) {
            return [].concat(_toConsumableArray(result), [encode(key, options)]);
          }

          return [].concat(_toConsumableArray(result), [[encode(key, options), '=', encode(value, options)].join('')]);
        };
      };
  }
}

function parserForArrayFormat(options) {
  var result;

  switch (options.arrayFormat) {
    case 'index':
      return function (key, value, accumulator) {
        result = /\[(\d*)\]$/.exec(key);
        key = key.replace(/\[\d*\]$/, '');

        if (!result) {
          accumulator[key] = value;
          return;
        }

        if (accumulator[key] === undefined) {
          accumulator[key] = {};
        }

        accumulator[key][result[1]] = value;
      };

    case 'bracket':
      return function (key, value, accumulator) {
        result = /(\[\])$/.exec(key);
        key = key.replace(/\[\]$/, '');

        if (!result) {
          accumulator[key] = value;
          return;
        }

        if (accumulator[key] === undefined) {
          accumulator[key] = [value];
          return;
        }

        accumulator[key] = [].concat(accumulator[key], value);
      };

    case 'comma':
      return function (key, value, accumulator) {
        var isArray = typeof value === 'string' && value.split('').indexOf(',') > -1;
        var newValue = isArray ? value.split(',') : value;
        accumulator[key] = newValue;
      };

    default:
      return function (key, value, accumulator) {
        if (accumulator[key] === undefined) {
          accumulator[key] = value;
          return;
        }

        accumulator[key] = [].concat(accumulator[key], value);
      };
  }
}

function encode(value, options) {
  if (options.encode) {
    return options.strict ? strictUriEncode(value) : encodeURIComponent(value);
  }

  return value;
}

function decode(value, options) {
  if (options.decode) {
    return decodeComponent(value);
  }

  return value;
}

function keysSorter(input) {
  if (Array.isArray(input)) {
    return input.sort();
  }

  if (_typeof(input) === 'object') {
    return keysSorter(Object.keys(input)).sort(function (a, b) {
      return Number(a) - Number(b);
    }).map(function (key) {
      return input[key];
    });
  }

  return input;
}

function removeHash(input) {
  var hashStart = input.indexOf('#');

  if (hashStart !== -1) {
    input = input.slice(0, hashStart);
  }

  return input;
}

function extract(input) {
  input = removeHash(input);
  var queryStart = input.indexOf('?');

  if (queryStart === -1) {
    return '';
  }

  return input.slice(queryStart + 1);
}

function parseValue(value, options) {
  if (options.parseNumbers && !Number.isNaN(Number(value)) && typeof value === 'string' && value.trim() !== '') {
    value = Number(value);
  } else if (options.parseBooleans && value !== null && (value.toLowerCase() === 'true' || value.toLowerCase() === 'false')) {
    value = value.toLowerCase() === 'true';
  }

  return value;
}

function parse(input, options) {
  options = Object.assign({
    decode: true,
    sort: true,
    arrayFormat: 'none',
    parseNumbers: false,
    parseBooleans: false
  }, options);
  var formatter = parserForArrayFormat(options); // Create an object with no prototype

  var ret = Object.create(null);

  if (typeof input !== 'string') {
    return ret;
  }

  input = input.trim().replace(/^[?#&]/, '');

  if (!input) {
    return ret;
  }

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = input.split('&')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var param = _step.value;

      var _splitOnFirst = splitOnFirst(param.replace(/\+/g, ' '), '='),
          _splitOnFirst2 = _slicedToArray(_splitOnFirst, 2),
          key = _splitOnFirst2[0],
          value = _splitOnFirst2[1]; // Missing `=` should be `null`:
      // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters


      value = value === undefined ? null : decode(value, options);
      formatter(decode(key, options), value, ret);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  for (var _i = 0, _Object$keys = Object.keys(ret); _i < _Object$keys.length; _i++) {
    var key = _Object$keys[_i];
    var value = ret[key];

    if (_typeof(value) === 'object' && value !== null) {
      for (var _i2 = 0, _Object$keys2 = Object.keys(value); _i2 < _Object$keys2.length; _i2++) {
        var k = _Object$keys2[_i2];
        value[k] = parseValue(value[k], options);
      }
    } else {
      ret[key] = parseValue(value, options);
    }
  }

  if (options.sort === false) {
    return ret;
  }

  return (options.sort === true ? Object.keys(ret).sort() : Object.keys(ret).sort(options.sort)).reduce(function (result, key) {
    var value = ret[key];

    if (Boolean(value) && _typeof(value) === 'object' && !Array.isArray(value)) {
      // Sort object keys, not values
      result[key] = keysSorter(value);
    } else {
      result[key] = value;
    }

    return result;
  }, Object.create(null));
}

exports.extract = extract;
exports.parse = parse;

exports.stringify = function (object, options) {
  if (!object) {
    return '';
  }

  options = Object.assign({
    encode: true,
    strict: true,
    arrayFormat: 'none'
  }, options);
  var formatter = encoderForArrayFormat(options);
  var keys = Object.keys(object);

  if (options.sort !== false) {
    keys.sort(options.sort);
  }

  return keys.map(function (key) {
    var value = object[key];

    if (value === undefined) {
      return '';
    }

    if (value === null) {
      return encode(key, options);
    }

    if (Array.isArray(value)) {
      return value.reduce(formatter(key), []).join('&');
    }

    return encode(key, options) + '=' + encode(value, options);
  }).filter(function (x) {
    return x.length > 0;
  }).join('&');
};

exports.parseUrl = function (input, options) {
  return {
    url: removeHash(input).split('?')[0] || '',
    query: parse(extract(input), options)
  };
};

/***/ }),

/***/ "./node_modules/split-on-first/index.js":
/*!**********************************************!*\
  !*** ./node_modules/split-on-first/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (string, separator) {
  if (!(typeof string === 'string' && typeof separator === 'string')) {
    throw new TypeError('Expected the arguments to be of type `string`');
  }

  if (separator === '') {
    return [string];
  }

  var separatorIndex = string.indexOf(separator);

  if (separatorIndex === -1) {
    return [string];
  }

  return [string.slice(0, separatorIndex), string.slice(separatorIndex + separator.length)];
};

/***/ }),

/***/ "./node_modules/strict-uri-encode/index.js":
/*!*************************************************!*\
  !*** ./node_modules/strict-uri-encode/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function (x) {
    return "%".concat(x.charCodeAt(0).toString(16).toUpperCase());
  });
};

/***/ }),

/***/ "./src/js/components/nav-accordion.js":
/*!********************************************!*\
  !*** ./src/js/components/nav-accordion.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() {
	var accordionToggle = document.querySelectorAll('.js-nav-accordion__toggle');

	accordionToggle.forEach(function (el) {
		el.addEventListener('click', function(e) {
			e.preventDefault();

			if (!Element.prototype.matches) {
				Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
			}

			if (!Element.prototype.closest) {
				Element.prototype.closest = function(s) {
					var el = this;

					do {
						if (el.matches(s)) return el;
						el = el.parentElement || el.parentNode;
					} while (el !== null && el.nodeType === 1);
					return null;
				};
			}

			this.closest(('.js-nav-accordion')).classList.toggle('is-active');
		});
	});
})();


/***/ }),

/***/ "./src/js/components/nav-highlight.js":
/*!********************************************!*\
  !*** ./src/js/components/nav-highlight.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _url_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./url-manager */ "./src/js/components/url-manager.js");


// ---------------------------------
// nav keyboard navigation and
// active template highlighting
// ---------------------------------


let links = document.querySelectorAll('.js-nav-iframe li a:not(.js-nav-accordion__toggle)');
let validLinks = [];
let navigate;
// separate inactive and valid links
links.forEach(function (link) {
  validLinks.push(link);
});

const enhanceNav = () => {
  // when changing active link, remove is-active class from the last one
  const activeLink = document.querySelectorAll('.js-nav-iframe li a.is-active');
  if (activeLink.length) {
    activeLink[0].classList.remove('is-active');
  }

  // check which template is active
  let currentUrl = window.location.href; // whole url in omnibox
  let currentPage = Object(_url_manager__WEBPACK_IMPORTED_MODULE_0__["dissectUrl"])(currentUrl).pageParameter; // parameter page

  // determine active link and its position in an array
  let currentLink = undefined;
  let currentLinkPos = undefined;
  validLinks.forEach(function (validLink, i) {
    if (validLink.href === currentPage) {
      currentLink = validLink;
      currentLinkPos = i;
      currentLink.classList.add("is-active");
    }
  })

  if (document.querySelectorAll(".js-nav-keyboard").length) {
    let prevLink = validLinks[currentLinkPos - 1];
    let nextLink = validLinks[currentLinkPos + 1];

    // keyboard controls
    navigate = function navigate(e) {
      if (e.which == 37) {
        if (prevLink && (currentLinkPos > 0)) {
          Object(_url_manager__WEBPACK_IMPORTED_MODULE_0__["setUrl"])(prevLink.href);
        }
      }
      if (e.which == 39) {
        if (nextLink) {
          Object(_url_manager__WEBPACK_IMPORTED_MODULE_0__["setUrl"])(nextLink.href);
        }
      }
    }
  } else {
    console.info("Keyboard template navigation is disabled. You can enable it in package settings. See gulp-sk-styleguide documentation on npm.");
  }
};
window.addEventListener('keydown', function () {
  navigate(event)
});
enhanceNav();

// when url changes, update navigation's active styles and pick corresponding prev/next links
window.addEventListener('urlChange', enhanceNav);


/***/ }),

/***/ "./src/js/components/nav-tools.js":
/*!****************************************!*\
  !*** ./src/js/components/nav-tools.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ---------------------------------
// Grid View Toggling
// ---------------------------------
var iframe = document.getElementById('my-iframe');

(function () {
  var body = document.querySelectorAll('body');
  var gridToggle = document.querySelectorAll('.js-toggle-grid-view');

  // toggle local storage value + update state
  var toggleGridView = function () {
    var currentGridViewStatus = localStorage.getItem('gridView');
    if (currentGridViewStatus == null) {
      localStorage.setItem('gridView', 1);
      setGridView();
    } else {
      localStorage.removeItem('gridView');
      setGridView();
    }
  }

  // set state
  var setGridView = function () {
    var currentGridViewStatus = localStorage.getItem('gridView');
    if (currentGridViewStatus == 1) {
      body[0].classList.add('grid-view');
      gridToggle[1].setAttribute('aria-checked', 'true');
    } else {
      body[0].classList.remove('grid-view');
      gridToggle[1].setAttribute('aria-checked', 'false');
    }
  }

	// init
	if (gridToggle.length !== 0) {
		document.addEventListener('DOMContentLoaded', (event) => {
			setGridView();
		});

		// listen for user input
		gridToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				toggleGridView();
			});
		});
	}


  // ---------------------------------
  // Debug View Toggling
  // ---------------------------------

  var debugToggle = document.querySelectorAll('.js-toggle-debug-view');

  // toggle local storage value + update state
  var toggleDebugView = function () {
    var currentDebugViewStatus = localStorage.getItem('debugView');
    if (currentDebugViewStatus == null) {
      localStorage.setItem('debugView', 1);
      setDebugView();
    } else {
      localStorage.removeItem('debugView');
      setDebugView();
    }
  }

  // set state
  var setDebugView = function () {
    var currentDebugViewStatus = localStorage.getItem('debugView');
	var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;

    if (currentDebugViewStatus == 1) {
	  frameDocument.querySelector('body').classList.add('debug-view');
	  if (debugToggle[1] != null) {
			debugToggle[1].setAttribute('aria-checked', 'true');
	  }

    } else {
      frameDocument.querySelector('body').classList.remove('debug-view');
			if (debugToggle[1] != null) {
				debugToggle[1].setAttribute('aria-checked', 'false');
			}
    }
  }

	// listen for user input
	if (debugToggle.length !== 0) {
		debugToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				toggleDebugView();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setDebugView();
		});
	}


  // ---------------------------------
  // Pin Nav Toggling
  // ---------------------------------
  var pinToggle = document.querySelectorAll('.js-toggle-pin-nav');

  // toggle local storage value + update state
  var togglePinNav = function () {
    var currentPinNavStatus = localStorage.getItem('pinNav');
    if (currentPinNavStatus == null) {
      localStorage.setItem('pinNav', 1);
      setPinNav();
    } else {
      localStorage.removeItem('pinNav');
      setPinNav();
    }
  }

  // set state
  var setPinNav = function () {
    var currentPinNavStatus = localStorage.getItem('pinNav');
    if (currentPinNavStatus == 1) {
      body[0].classList.add('pin-nav');
      pinToggle[1].setAttribute('aria-checked', 'true');
    } else {
      body[0].classList.remove('pin-nav');
      pinToggle[1].setAttribute('aria-checked', 'false');
    }
  }

	// listen for user input
	if (pinToggle.length !== 0) {
		pinToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				togglePinNav();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setPinNav();
		});
	}

  // ---------------------------------
  // Theme Switching
  // ---------------------------------
  var themeSwitchToggle = document.querySelectorAll('.js-toggle-theme-switch');

  // toggle local storage value + update state
  var themeSwitchNav = function () {
    var currentThemeSwitchStatus = localStorage.getItem('themeSwitch');
    if (currentThemeSwitchStatus == null) {
      localStorage.setItem('themeSwitch', 1);
      setThemeSwitch();
    } else {
      localStorage.removeItem('themeSwitch');
      setThemeSwitch();
    }
  }

  // set state
  var setThemeSwitch = function () {
		var iframeArray = [];
		var currentThemeSwitchStatus = localStorage.getItem('themeSwitch');
		if (document.body.contains(document.getElementById('my-iframe'))) {
			iframe = document.getElementById('my-iframe');

			// if (iframe.classList.contains(document.getElementsByClassName('docs-component__preview'))) {
			// 	iframeArray = document.getElementsByClassName('docs-component__preview');
			// }
		}
		else {
			return;
		}
		var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
		// var frameDocumentNested = frameDocument.contentDocument ? frameDocument.contentDocument : frameDocument.contentWindow.document;
		// console.log(frameDocumentNested);
		var stylesheetPrimary = frameDocument.getElementById("stylesheet-primary");
		var stylesheetSecondary = frameDocument.getElementById("stylesheet-secondary");

		// check if there is a page/component, that can be switched
		var stylesheetTogglingAvailable = false;
		if (stylesheetPrimary != null && stylesheetSecondary != null) {
			stylesheetTogglingAvailable = true;
		}

    if (!stylesheetTogglingAvailable ) {
			themeSwitchToggle[1].setAttribute('disabled', '');
		} else {
			themeSwitchToggle[1].removeAttribute('disabled');
			if (currentThemeSwitchStatus == 1) {
				// body[0].classList.add('pin-nav');
				stylesheetPrimary.disabled = true;
				stylesheetSecondary.disabled = false;
				themeSwitchToggle[1].setAttribute('aria-checked', 'true');
			} else {
				// body[0].classList.remove('pin-nav');
				stylesheetPrimary.disabled = false;
				stylesheetSecondary.disabled = true;
				themeSwitchToggle[1].setAttribute('aria-checked', 'false');
			}
		}
  }

	if (themeSwitchToggle.length !== 0) {
		// listen for user input
		themeSwitchToggle.forEach(function (el) {
			el.addEventListener('click', (event) => {
				themeSwitchNav();
			});
		});

		document.addEventListener('DOMContentLoaded', (event) => {
			setThemeSwitch();
		});
	}


  // init in iframe
  var handleLoad = () => {
		if (debugToggle.length !== 0) {
			setDebugView();
		}
		if (themeSwitchToggle.length !== 0) {
			setThemeSwitch();
		}

    var frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
    var iframeTitle = frameDocument.getElementsByTagName("title")[0].innerText;
    document.title = iframeTitle;

    var element = document.createElement("link");
    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", "../styleguide/css/sk-styleguide-dev-suite.css"); // todo dynamically
    frameDocument.head.appendChild(element);
  };

  if (iframe) {
    iframe.addEventListener('load', handleLoad, true);
  }
})();


/***/ }),

/***/ "./src/js/components/url-manager.js":
/*!******************************************!*\
  !*** ./src/js/components/url-manager.js ***!
  \******************************************/
/*! exports provided: dissectUrl, setUrl, changeUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dissectUrl", function() { return dissectUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setUrl", function() { return setUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeUrl", function() { return changeUrl; });
/* harmony import */ var _superkoders_sk_tools_src_event__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @superkoders/sk-tools/src/event */ "./node_modules/@superkoders/sk-tools/src/event.js");
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! query-string */ "./node_modules/query-string/index.js");
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(query_string__WEBPACK_IMPORTED_MODULE_1__);



var iframe = document.getElementById('my-iframe');
let urlChange = new Event('urlChange', { bubbles: true });

const dissectUrl = url => {
  const referrer = location.href;
  const urlParts = location.href.split('?');
  let urlParams = Object(query_string__WEBPACK_IMPORTED_MODULE_1__["parse"])(urlParts[1] || '');
  // let urlTarget = urlParts[0] + '?' + stringify({ ...urlParams, page: url }, { encode: false });
  let urlTarget = urlParts[0] + '?' + Object(query_string__WEBPACK_IMPORTED_MODULE_1__["stringify"])(Object.assign({}, urlParams, { page: url }), { encode: false });
  let urlObj = {
    urlSrc: url, // na co jsem kliknul, kam mirim (pro iframe)
    referrer: referrer, // z jake stranky odchazim
    urlParts: urlParts, // pole, kde je [root, parametry stranky, ze ktere odchazim]
    urlParams: urlParams, // pole vsech parametru stranky, ze ktere odchazim
    pageParameter: urlParams.page, // vypreparovany parametr page
    urlTarget: urlTarget // root + retezec parametru s novym 'page' parametrem; nova kompletni url adresa (pro omnibox)
  };

  return urlObj;
}

const setUrl = url => { // zmen URL v adresarovem radku, nastav historii
  const urlObj = dissectUrl(url);
  const referrer = urlObj.referrer;
  const urlTarget = urlObj.urlTarget;

  history.pushState({ urlObj, referrer, ajaxify: true }, null, urlTarget);
  changeUrl(history.state);
};

const changeUrl = ({ urlObj }) => { // aktivuj novou URL
  const urlSrc = urlObj.urlSrc;
  const pageParameter = urlObj.pageParameter;

  if (urlSrc != undefined) {
    iframe.contentWindow.location.replace(urlSrc); // zmen adresu na target
  } else if (pageParameter != undefined) {
    iframe.contentWindow.location.replace(pageParameter); // pri nacteni zadny target neni; pouzijeme adresu v parametru
  } else {
	iframe.contentWindow.location.replace(iframe.src);
    return; // pri nacteni domovske stranky nedelej nic, nech puvodni iframe.src
  }
  document.dispatchEvent(urlChange);
};

// init state
const urlInit = function () {
  const url = location.href; // e.g. http://localhost:3000/styleguide/?page=http://localhost:3000/styleguide/about-styleguide.html
  const urlObj = dissectUrl();
  history.replaceState({ urlObj, referrer: null, ajaxify: true }, null, url);
  changeUrl(history.state);

  // events
  Object(_superkoders_sk_tools_src_event__WEBPACK_IMPORTED_MODULE_0__["on"])(window, 'popstate', (event) => {
    if (event.state == null) { return; }
    changeUrl(event.state);
  });

  Object(_superkoders_sk_tools_src_event__WEBPACK_IMPORTED_MODULE_0__["on"])(document, 'click', Object(_superkoders_sk_tools_src_event__WEBPACK_IMPORTED_MODULE_0__["delegate"])('.js-nav-iframe a', (event, node) => {
    if (node.href.indexOf('/admin/') !== -1) {
      return;
    }
    event.preventDefault();
    setUrl(node.href); // e.g. http://localhost:3000/styleguide/icons.html
  }));
};
urlInit();



/***/ }),

/***/ "./src/js/styleguide.js":
/*!******************************!*\
  !*** ./src/js/styleguide.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_url_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/url-manager */ "./src/js/components/url-manager.js");
/* harmony import */ var _components_nav_tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/nav-tools */ "./src/js/components/nav-tools.js");
/* harmony import */ var _components_nav_tools__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_nav_tools__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_nav_highlight__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/nav-highlight */ "./src/js/components/nav-highlight.js");
/* harmony import */ var _components_nav_accordion__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/nav-accordion */ "./src/js/components/nav-accordion.js");
/* harmony import */ var _components_nav_accordion__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_nav_accordion__WEBPACK_IMPORTED_MODULE_3__);





const components = [
  _components_url_manager__WEBPACK_IMPORTED_MODULE_0__,
  _components_nav_tools__WEBPACK_IMPORTED_MODULE_1__,
  _components_nav_accordion__WEBPACK_IMPORTED_MODULE_3__,
  _components_nav_highlight__WEBPACK_IMPORTED_MODULE_2__
];

window.App = {
  run() {
    var target = document;
    components.forEach((component) => component.init(target));
  },

  initComponent(component) {
    return component.init();
  },
};


/***/ })

/******/ });